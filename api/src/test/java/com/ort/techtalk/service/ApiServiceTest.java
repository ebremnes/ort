package com.ort.techtalk.service;

import com.ort.techtalk.model.Auth;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ApiServiceTest {


    @InjectMocks
    ApiService service;

    @Mock
    Auth auth;

    @BeforeEach
    void setUp() {
        auth = Auth.builder()
                .bearer("1234567")
                .refresh("1234567")
                .ttl("1234567")
                .email("email@email.com")
                .user("student1")
                .build();

        service = new ApiService();
    }


    @Test
    void testAuth() {
        ResponseEntity<Auth> response = service.auth(auth.getUser(), auth.getEmail());
        assertFalse(response.getBody().getBearer().isEmpty());
        assertFalse(response.getBody().getRefresh().isEmpty());
    }

    @Test
    void testRefresh() {
        ResponseEntity<Auth> response = service.refresh(auth.getBearer(), auth.getRefresh(), auth.getTtl());
        assertFalse(response.getBody().getBearer().isEmpty());
        assertFalse(response.getBody().getRefresh().isEmpty());
    }

    @Test
    void shouldFailWhenNoUSerPresentHappyPath() {
        ResponseEntity<Auth> response = service.auth(null, auth.getEmail());
        assertEquals("Missing user", response.getBody().getError());
    }

    @Test
    void shouldFailWhenNoEmailPresentHappyPath() { }

    @Test
    void shouldFailWhenNoBearerRefreshTtlPresentHappyPath() {

        //case bearer

        //case refresh

        //case ttl

    }

}