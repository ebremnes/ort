package com.ort.techtalk.service;

import com.ort.techtalk.model.Auth;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class ApiService {

    final String bearer = "&1234567890abcdefg";
    final String refresh = "$$1234567890abcdefg";
    final long MAXDAYS = 1209600000; //For testing set -1 (yesterday)

    /**
     * Create a {@link Auth} model for approved user
     *
     * @param user
     * @param email
     * @return
     */
    public ResponseEntity<Auth> auth(String user, String email) {

        try {

            //This is only a test on how we implement exception
            if (ObjectUtils.isEmpty(user)) {
                throw new Exception("Missing user");
            }

            if (ObjectUtils.isEmpty(email)) {
                throw new Exception("Missing email");
            }

            return ResponseEntity.ok().body(Auth.builder()
                    .bearer(bearer)
                    .refresh(refresh)
                    .email(email)
                    .user(user)
                    .ttl(String.valueOf(System.currentTimeMillis() + MAXDAYS))
                    .build());

        } catch (Exception exception) {
            //This is a mock service and we really don't do anything here HAPPY PATH
            return ResponseEntity.ok().body(Auth.builder().error(exception.getMessage()).build());
        }

    }

    /**
     * Refresk bearer token for approved user
     *
     * @param authorization
     * @param refresh
     * @param ttl
     * @return
     */
    public ResponseEntity<Auth> refresh(String authorization, String refresh, String ttl) {

        try {

            //This is only a test on how we implement exception
            if (ObjectUtils.isEmpty(authorization)) {
                throw new Exception("Missing authorization");
            }

            if (ObjectUtils.isEmpty(refresh)) {
                throw new Exception("Missing refresh");
            }

            if (ObjectUtils.isEmpty(ttl)) {
                throw new Exception("Missing TTL");
            }

            return ResponseEntity.ok().body(Auth.builder()
                    .bearer(bearer)
                    .refresh(refresh)
                    .ttl(String.valueOf(System.currentTimeMillis() + MAXDAYS))
                    .build());

        } catch (Exception exception) {
            //This is a mock service and we really don't do anything here HAPPY PATH
            return ResponseEntity.ok().body(Auth.builder().error(exception.getMessage()).build());
        }

    }
}
