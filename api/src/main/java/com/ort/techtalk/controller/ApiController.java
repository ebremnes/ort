package com.ort.techtalk.controller;

import com.ort.techtalk.controller.api.ApiClient;
import com.ort.techtalk.model.Auth;
import com.ort.techtalk.service.ApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ApiController implements ApiClient {

    /**
     * {@link ApiService}
     */
    private final ApiService service;

    @Override
    public ResponseEntity<Auth> auth(String user, String email) {
        return service.auth(user, email);
    }

    @Override
    public ResponseEntity<Auth> refresh(String authorization, String refresh, String ttl) {
        return service.refresh(authorization, refresh, ttl);
    }
}
