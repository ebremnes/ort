package com.ort.techtalk.controller.api;

import com.ort.techtalk.model.Auth;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "ort-client")
public interface ApiClient {

    @GetMapping("api/auth")
    @CrossOrigin("http://localhost:8100")
    ResponseEntity<Auth> auth(
            @RequestHeader("user") String user,
            @RequestHeader("email") String email
    );

    @GetMapping("api/refresh")
    @CrossOrigin("http://localhost:8100")
    ResponseEntity<Auth> refresh(
            @RequestHeader("authorization") String authorization,
            @RequestHeader("refresh") String refresh,
            @RequestHeader("ttl") String ttl
    );
}
