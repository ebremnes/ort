package com.ort.techtalk.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Auth {
    private String bearer;
    private String refresh;
    private String ttl;
    private String user;
    private String email;
    private String error;
}
