import { Component, OnInit } from '@angular/core';
import { StorageService } from '../provider/storage.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private storage: StorageService, private router: Router) { }

  ngOnInit() {
    this.storage.getAuth().then((valid: Auth) => {
      //We dont have valid auth
      if (!valid) {
        console.log("We dont have valid auth");
        this.router.navigateByUrl('home');
      }
      //We have valid auth in storage
      // TTL should allow 14 days
      // Tokens must be present
      else if (valid.ttl > Date.now() && valid.bearer && valid.refresh) {
        console.log("We have valid auth in storage : TTL should allow 14 days : Tokens must be present");
        this.router.navigateByUrl('welcome');
      }
      //We have to refresh
      else if (valid.ttl < Date.now()) {
        console.log("We have to refresh");
        this.router.navigateByUrl("home");
      }
    })
  }

  logout() {
    this.storage.removeAuth()
      .then(() => this.router.navigateByUrl("home"))
  }
}
