import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }

  /**
   * Get auth from storage
   */
  async getAuth() {
    return this.storage.get('auth');
  }

  /**
   * Set auth to storage
   * @param auth 
   */
  async setAuth(auth: { user: any, email: any, bearer: any, refresh: any, ttl: number }) {
    return this.storage.set('auth', auth);
  }

  /**
   * Use remove to logut
   */
  async removeAuth(){
    return this.storage.remove('auth');
  }
}
