import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: Http) { }

  /**
   * Example api to get creds from backend
   * @param data {}
   */
  getAuthFromApi(data: Auth): Observable<any> {

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('authorization', 'bearer ' + data.bearer || undefined);
    headers.append('refresh', data.refresh || undefined);
    headers.append('ttl', data.ttl.toString());
    headers.append('user', data.user || undefined);
    headers.append('email', data.email || undefined);

    let requestOptionsArgs = new RequestOptions({ headers: headers });

    return this.http.get("http://localhost:8090/api/auth", requestOptionsArgs);
  }

  /**
 * Example api to get new creds from backend
 * @param data {}
 */
  getRefreshFromApi(data: Auth): Observable<any> {

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('authorization', 'bearer ' + data.bearer || undefined);
    headers.append('refresh', data.refresh || undefined);
    headers.append('ttl', data.ttl.toString());

    let requestOptionsArgs = new RequestOptions({ headers: headers });

    return this.http.get("http://localhost:8090/api/refresh", requestOptionsArgs);
  }


}
