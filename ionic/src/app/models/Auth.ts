interface Auth {
    user: any,
    email: any,
    bearer: any,
    refresh: any,
    ttl: number,
    error: any,
}