import { Component } from '@angular/core';
import { Router } from '@angular/router'
import { StorageService } from '../provider/storage.service';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../provider/auth.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  data: Auth;
  error: any;

  constructor(private router: Router, private storage: StorageService, private auth: AuthService) {
    //default
    this.data = { user: "", email: "", bearer: "", refresh: "", ttl: 0, error: "" };
  }


  login(form: FormGroup) {

    try {

      // A  auth value should be stored in the app
      this.storage.getAuth()
        .then((valid: Auth) => {

          //We have no valid auth in storage but we have form data
          if (!valid && form.value) {

            //Insert form data
            this.data.user = form.value.user;
            this.data.email = form.value.email;

            // call api and try to get tokens
            this.auth.getAuthFromApi(this.data).subscribe(response => {

              //Incoming response from API
              let auth: Auth = JSON.parse(response._body)

              //Check for bad Request HAPPY PATH
              if (auth.error) {
                this.error = auth.error;
              } else {
                //We're good to go
                // Tokens must be saved
                this.storage.setAuth(auth)
                  //redirect to home page
                  .then(() => this.router.navigateByUrl('welcome'))
                  .catch();
              }

            });;

          }

          //We do not have any TTL valid anymore, ask for new
          else if (valid.bearer && valid.refresh) {

            //We know valid has all we need, so just send valid
            this.auth.getRefreshFromApi(valid).subscribe(response => {

              //Incoming response from API
              let auth: Auth = JSON.parse(response._body)
              valid.bearer = auth.bearer;
              valid.refresh = auth.refresh;
              valid.ttl = auth.ttl;

              //Check for bad Request HAPPY PATH
              if (auth.error) {
                this.error = auth.error;
              } else {
                //Storage
                this.storage.setAuth(valid)
                  .then(() => this.router.navigateByUrl('welcome'))
                  .catch();
              }

            });
          }
        })
        .catch();
    }
    catch (error) { }
  }
}
